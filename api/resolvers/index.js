const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const Product = require("../../models/product");
const User = require("../../models/user");

const user = (userId) => {
  return User.findById(userId)
    .then((user) => {
      return {
        ...user._doc,
        _id: user.id,
        created_products: products.bind(this, user._doc.created_products),
      };
    })
    .catch((err) => {
      throw err;
    });
};

const products = (productIds) => {
  return Product.find({ _id: { $in: productIds } })
    .then((products) => {
      return products.map((product) => {
        return transformProduct(product);
      });
    })
    .catch((err) => {
      throw err;
    });
};

//transforms product from mongoose Schema to a GraphQL Type
const transformProduct = (product) => {
  return {
    ...product._doc,
    _id: product.id,
    owner: user.bind(this, product._doc.owner),
  };
};

module.exports = {
  products: () => {
    return Product.find()
      .then((products) => {
        return products.map((product) => {
          return transformProduct(product);
        });
      })
      .catch((err) => {
        throw err;
      });
  },

  user: async (args, req) => {
    try {
      const result = await User.findById(req.userId);
      return {
        ...result._doc,
        password: null,
        _id: result.id,
        created_products: products.bind(this, result._doc.created_products),
      };
    } catch (err) {
      throw err;
    }
  },
  createProduct: async (args, req) => {
    if (!req.isAuth) {
      throw new Error("You must be authenticated to gain access!");
    }
    const product = new Product({
      name: args.productInput.name,
      version: args.productInput.version,
      build: args.productInput.build,
      build_type: args.productInput.build_type,
      release_notes: args.productInput.release_notes,
      download_link: args.productInput.download_link,
      owner: req.userId,
    });
    let createdProduct;
    try {
      const result = await product.save();
      createdProduct = transformProduct(result);
      const owner = await User.findById(req.userId);
      if (!owner) {
        throw new Error("User not found.");
      }

      //push the product to the DB
      owner.created_products.push(product);
      await owner.save();
      return createdProduct;
    } catch (err) {
      console.log(err);
      throw err;
    }
  },

  updateProduct: async (args, req) => {
    await Product.update(
      {
        _id: args.productId,
      },
      {
        $set: {
          name: args.productInput.name,
          version: args.productInput.version,
          build: args.productInput.build,
          build_type: args.productInput.build_type,
          release_notes: args.productInput.release_notes,
          download_link: args.productInput.download_link,
        },
      }
    );
    let updatedProduct;
    try {
      const result = await Product.findById(args.productId);
      updatedProduct = transformProduct(result);
      return result;
    } catch (err) {
      throw err;
    }
  },

  deleteProduct: async (args) => {
    if (!req.isAuth) {
      throw new Error("You must be authenticated to gain access!");
    }
    try {
      await Product.findByIdAndRemove(args.productId);
      return true;
    } catch (err) {
      throw err;
    }
  },

  createUser: (args) => {
    return User.findOne({ username: args.userInput.username })
      .then((user) => {
        if (user) {
          throw new Error("User already exists");
        }
        return bcrypt.hash(args.userInput.secret_code, 12);
      })
      .then((hashedCode) => {
        const user = new User({
          username: args.userInput.username,
          secret_code: hashedCode,
        });
        return user.save();
      })
      .then((result) => {
        // Overriding the retrieved password with null for security.
        return { ...result._doc, password: null, _id: result.id };
      })
      .catch((err) => {
        throw err;
      });
  },

  login: async ({ username, secret_code }) => {
    const loginuser = await User.findOne({ username: username });
    if (!loginuser) {
      throw new Error("User does not exist");
    }
    const isEqual = await bcrypt.compare(secret_code, loginuser.secret_code);
    if (!isEqual) {
      throw new Error("Sorry, that's not the right secret code");
    }
    // Add data to the token
    const token = jwt.sign(
      { userId: loginuser.id, username: loginuser.username },
      "thatkey",
      {
        expiresIn: "1h",
      }
    );
    return { userId: loginuser.id, token: token, tokenExpiration: 1 };
  },
};
