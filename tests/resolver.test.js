const request = require("supertest");
const express = require("express");
const app = require("../app").default;

describe("Testing GraphQL", () => {
  it("Returns all products", (done) => {
    request(app)
      .post("/api")
      .send({ query: "query{ products{ _id name version } }" })
      .expect(200)
      .end((err, res) => {
        // res will contain array of all users
        if (err) return done(err);
        // assume there are a 5 users in the database
        //change to should be an array
        res.body.user.should.have.lengthOf(5);
      });
  });
});
