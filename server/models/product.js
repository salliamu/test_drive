const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const productSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  version: {
    type: String,
    required: true,
  },
  build: {
    type: String,
    required: true,
  },
  build_type: {
    type: String,
    required: true,
    enum: ["stable", "beta", "nightly"],
  },
  release_notes: {
    type: String,
    required: true,
  },
  download_link: {
    type: String,
    required: true,
  },
  owner: {
    type: Schema.Types.ObjectId,
    ref: "User",
  },
});

module.exports = mongoose.model("Product", productSchema);
