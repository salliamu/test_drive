const chai = require("chai");
const expect = chai.expect;
const url = `http://localhost:8000`;
const request = require("supertest")(url);

describe("Testing GraphQL", () => {
  it("Returns all products", (done) => {
    request
      .post("/api")
      .send({ query: "{ products{ _id name version } }" })
      .expect(200)
      .end(async (err, res) => {
        if (err) return done(err);
        const result = await res.body.data.products;
        expect(result).to.be.an("array"), done();
      });
  });

  it("Logs in user", (done) => {
    request
      .post("/api")
      .send({
        query: `query{login(username: "Kai", secret_code:"12345"){ userId token }}`,
      })
      .expect(200)
      .end(async (err, res) => {
        if (err) return done(err);
        expect(res.body.data.login).to.have.property("token"), done();
      });
  });

  it("Creates a new user", (done) => {
    request
      .post("/api")
      .send(
        {
          query: `mutation {
          createUser(userInput:{ username: "Dan", secret_code:"pennies"}){
              username
            }
          }`,
        },
        done
      )
      .expect(200)
      .end(async (err, res) => {
        if (err) return done(err);
        expect(res.body.data.createUser).to.have.property("username"), done();
      });
  });

  it("Throw error due to invalid username", (done) => {
    request
      .post("/api")
      .send(
        {
          query: `
          mutation {
            createUser(userInput:{ username: "Dory", secret_code:"spin"}){
                username
              }
            }
          `,
        },
        done
      )
      .expect(200)
      .end(async (err, res) => {
        if (err) return done(err);
        expect(res.body).to.have.property("errors"), done();
      });
  });

  it("Updates a product", (done) => {
    request
      .post("/api")
      .send({
        query: `
        mutation {
          updateProduct(productId:"5ec2adf70a209e04fea2b676", productInput: {name: "The Nanasi", version:"12345", build:"", build_type:"beta", release_notes:"", download_link:""}){
            name
            version
          }
        }`,
      })
      .expect(200)
      .end(async (err, res) => {
        if (err) return done(err);
        expect(res.body.data.updateProduct).to.have.property("name"), done();
      });
  });

  it("Deletes a product", (done) => {
    request
      .post("/api")
      .send({
        query: `
        mutation {
          deleteProduct(productId:"5ec46bcd705e851956ac9bf0")
        }`,
      })
      .expect(200)
      .end(async (err, res) => {
        if (err) return done(err);
        expect(res.body.data.deleteProduct).to.equal(true), done();
      });
  });
});
