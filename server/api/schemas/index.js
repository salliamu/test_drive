const { buildSchema } = require("graphql");

module.exports = buildSchema(`
type Product {
    _id: ID!
    name: String!
    version: String!
    build: String!
    build_type: String!
    release_notes: String!
    download_link: String!
    owner: User!
}

type User {
    _id: ID!
    username: String!
    secret_code: String
    created_products: [Product!]
}

type AuthData {
    userId: ID!
    token: String!
  }
  
input ProductInput {
    name: String!
    version: String!
    build: String!
    build_type: String!
    release_notes: String!
    download_link: String!
}

input UserInput{
    username: String!
    secret_code: String!
}

type RootQuery {
    products: [Product!]!
    login(username: String!, secret_code: String): AuthData! 
    userInfo(userId: ID): User!
}

type RootMutation {
    createUser(userInput: UserInput) : User
    createProduct(productInput: ProductInput): Product!
    updateProduct(productId: ID, productInput: ProductInput): Product!
    deleteProduct(productId: ID): Boolean
}
schema {
    query: RootQuery
    mutation: RootMutation
}
`);
