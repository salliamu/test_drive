const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const Product = require("../../models/product");
const User = require("../../models/user");

/**
 * Takes a user object retrieved from the database and transforms each field into
 * the proper types compatible with GraphQL.
 *
 * @param {Schema.Types.ObjectId} userId The ID of a user
 */
const user = (userId) => {
  return User.findById(userId)
    .then((user) => {
      return {
        ...user._doc,
        _id: user.id,
        created_products: products.bind(this, user._doc.created_products),
      };
    })
    .catch((err) => {
      throw err;
    });
};

/**
 * Retrieves the requested list of products from the database and transforms
 * the fields for each product into the proper types compatible with GraphQL.
 *
 * @param {Schema.Types.ObjectId} productIds A list of product ids
 */
const products = (productIds) => {
  return Product.find({ _id: { $in: productIds } })
    .then((products) => {
      return products.map((product) => {
        return transformProduct(product);
      });
    })
    .catch((err) => {
      throw err;
    });
};

/**
 * Transforms product from mongoose Schema to have compatible fields with GraphQL.
 * @param {MongooseSchema} product
 */
const transformProduct = (product) => {
  return {
    ...product._doc,
    _id: product.id,
    owner: user.bind(this, product._doc.owner),
  };
};

module.exports = {
  /**
   * Query that returns a list of all products in the database.
   */
  products: () => {
    return Product.find()
      .then((products) => {
        return products.map((product) => {
          return transformProduct(product);
        });
      })
      .catch((err) => {
        throw err;
      });
  },

  /**
   * A mutation that creates a new product.
   */
  createProduct: async (args, req) => {
    if (!req.isAuth) {
      throw new Error("You must be authenticated to gain access!");
    }
    const product = new Product({
      name: args.productInput.name,
      version: args.productInput.version,
      build: args.productInput.build,
      build_type: args.productInput.build_type,
      release_notes: args.productInput.release_notes,
      download_link: args.productInput.download_link,
      owner: req.userId,
    });
    let createdProduct;
    try {
      const result = await product.save();
      createdProduct = transformProduct(result);
      const owner = await User.findById(req.userId);
      if (!owner) {
        throw new Error("User not found.");
      }

      //push the product to the DB
      owner.created_products.push(product);
      await owner.save();
      return createdProduct;
    } catch (err) {
      console.log(err);
      throw err;
    }
  },

  /**
   * A mutation that allows an authenticated user to make changes to
   * an existing product.
   */
  updateProduct: async (args, req) => {
    await Product.update(
      {
        _id: args.productId,
      },
      {
        $set: {
          name: args.productInput.name,
          version: args.productInput.version,
          build: args.productInput.build,
          build_type: args.productInput.build_type,
          release_notes: args.productInput.release_notes,
          download_link: args.productInput.download_link,
        },
      }
    );
    let updatedProduct;
    try {
      const result = await Product.findById(args.productId);
      updatedProduct = transformProduct(result);
      return result;
    } catch (err) {
      throw err;
    }
  },

  /**
   * Allows an authenticated user to delete a product.
   */
  deleteProduct: async (req, args) => {
    console.log(args.productId);
    try {
      await Product.deleteOne({ _id: args.productId });
      return true;
    } catch (err) {
      throw err;
    }
  },

  /**
   * Allows a new user to sign up.
   */
  createUser: (args) => {
    return User.findOne({ username: args.userInput.username })
      .then((user) => {
        if (user) {
          throw new Error("User already exists");
        }
        return bcrypt.hash(args.userInput.secret_code, 12);
      })
      .then((hashedCode) => {
        const user = new User({
          username: args.userInput.username,
          secret_code: hashedCode,
        });
        return user.save();
      })
      .then((result) => {
        // Overriding the retrieved password with null for security.
        return { ...result._doc, password: null, _id: result.id };
      })
      .catch((err) => {
        throw err;
      });
  },

  /**
   * Handles login logic.
   */
  login: async ({ username, secret_code }) => {
    const loginuser = await User.findOne({ username: username });
    if (!loginuser) {
      throw new Error("User does not exist");
    }
    const isEqual = await bcrypt.compare(secret_code, loginuser.secret_code);
    if (!isEqual) {
      throw new Error("Sorry, that's not the right secret code");
    }
    // Add data to the token
    const token = jwt.sign(
      { userId: loginuser.id, username: loginuser.username },
      "thatkey",
      {
        expiresIn: "0.2h",
      }
    );
    return { userId: loginuser.id, token: token, tokenExpiration: 1 };
  },

  /**
   * A query that returns all information about a single user.
   */
  userInfo: async (args) => {
    try {
      const result = await User.findById(args.userId);
      return {
        ...result._doc,
        password: null,
        _id: result.id,
        created_products: products.bind(this, result._doc.created_products),
      };
    } catch (err) {
      throw err;
    }
  },
};
