const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  const authHeader = req.get("Authorization");

  /**
   * Adds metadata to the header, in order to let any interested parties
   * that its not authenticated.
   */
  if (!authHeader) {
    req.isAuth = false;
    return next();
  }

  /**
   * Splits the header in order to retrieve the token.
   */
  const token = authHeader.split(" ")[1]; // Authorization Bearer dfsadf
  if (!token || token === "") {
    req.isAuth = false;
    return next();
  }

  /**
   * Verfies the key in the token and adds the userID, so that it can be accessed
   * from the request.
   */
  let decodedToken;
  try {
    decodedToken = jwt.verify(token, "thatkey");
  } catch (err) {
    req.isAuth = false;
    return next();
  }
  if (!decodedToken) {
    req.isAuth = false;
    return next();
  }
  req.isAuth = true;
  req.userId = decodedToken.userId;
  next();
};
