import React, { Component } from "react";
import { Form, Segment, Header } from "semantic-ui-react";
import AuthContext from "../authContext";

class ProductForm extends Component {
  static contextType = AuthContext;
  render() {
    return (
      <Segment padded="very">
        <Header as="h2" className="padded">
          Add a New Release.
        </Header>
        <Form onSubmit={this.props.handleSubmit}>
          <Form.Group widths="equal">
            <Form.Input
              fluid
              label="Product name"
              placeholder="Product name"
              onChange={this.props.handleChange("name")}
              required
            />
            <Form.Input
              fluid
              label="Version"
              placeholder="v12.3.2"
              onChange={this.props.handleChange("version")}
            />
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Input
              fluid
              label="Build Number"
              placeholder="75637"
              onChange={this.props.handleChange("build")}
            />
            <Form.Input
              fluid
              label="Build Type"
              placeholder="Beta"
              onChange={this.props.handleChange("build_type")}
            />
          </Form.Group>
          <Form.TextArea
            label="Release Notes"
            placeholder="Tell us more about this release..."
            onChange={this.props.handleChange("release_notes")}
          />
          <Form.Input
            fluid
            label="Download Link"
            placeholder="https://my.downloads.com"
            onChange={this.props.handleChange("download_link")}
          />
          <Form.Button>Submit</Form.Button>
        </Form>
      </Segment>
    );
  }
}

export default ProductForm;
