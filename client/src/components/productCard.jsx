import React from "react";
import { Card, Icon, Label } from "semantic-ui-react";

const ProductCard = (props) => (
  <Card.Group key={props.productId}>
    <Card fluid color="teal">
      <Card.Content>
        <Label as="a" color="teal" ribbon="right">
          {props.build_type}
        </Label>
        <Card.Header>
          {props.name}
          <Label color="teal" horizontal>
            {props.version}
          </Label>
        </Card.Header>
        <Card.Meta>{props.build}</Card.Meta>
      </Card.Content>
      <Card.Content>
        <Card.Description>{props.release_notes}</Card.Description>
      </Card.Content>
      <Card.Content extra textAlign="right">
        <Icon name="cloud download" />
        {props.download_link}
      </Card.Content>
    </Card>
  </Card.Group>
);

export default ProductCard;
