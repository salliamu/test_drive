import React, { Component } from "react";
import { List, Button, Icon, Header, Modal } from "semantic-ui-react";
import AuthContext from "../authContext";

import ProductUpdate from "../components/productUpdate";

class ProductModal extends Component {
  state = {
    modalOpen: false,
    updatedFormData: {
      name: this.props.name,
      version: this.props.version,
      build: this.props.build,
      build_type: this.props.build_type,
      release_notes: this.props.release_notes,
      download_link: this.props.download_link,
    },
  };
  static contextType = AuthContext;

  handleOpen = () => this.setState({ modalOpen: true });
  handleClose = () => this.setState({ modalOpen: false });

  /**
   * Deletes a product.
   */
  handleDelete = () => {
    const reqBody = {
      query: `
      mutation {
        deleteProduct(productId: "${this.props.productId}")
      }
          `,
    };

    fetch("http://localhost:8000/api", {
      method: "POST",
      body: JSON.stringify(reqBody),
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.context.token,
      },
    })
      .then((res) => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then((resData) => {
        console.log(resData);
        const isDeleted = resData;
        this.setState({ modalOpen: !isDeleted });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  /**
   * Updates the array when user makes changes.
   */
  handleChange = (input) => (event) => {
    var updatedFormData = this.state.updatedFormData;
    updatedFormData[input] = event.target.value;
    this.setState(updatedFormData);
  };

  /**
   * Updates the api of changes made by logged in users.
   */
  handleChangeUpdate() {
    const formData = this.state.updatedFormData;
    const reqBody = {
      query: `
      mutation {
        updateProduct(productId: "${this.props.productId}",productInput: {name:"${formData.name}", version:"${formData.version}", build:"${formData.build}", build_type:"${formData.build_type}", release_notes:"${formData.release_notes}", download_link:"${formData.download_link}"}){
          name
          version
          build
          build_type
          release_notes
          download_link
        }
      }
          `,
    };

    fetch("http://localhost:8000/api", {
      method: "POST",
      body: JSON.stringify(reqBody),
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.context.token,
      },
    })
      .then((res) => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then((resData) => {
        console.log(resData);
        this.setState({ modalOpen: false });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  render() {
    const { updatedFormData } = this.state.updatedFormData;
    return (
      <Modal
        trigger={
          <List.Item key={this.props.productId} onClick={this.handleOpen}>
            <List.Icon name="folder" verticalAlign="middle" size="large" />
            <List.Header>{this.props.name}</List.Header>
            <List.Description>{this.props.version}</List.Description>
          </List.Item>
        }
        open={this.state.modalOpen}
        onClose={this.handleClose}
        basic
        size="small"
      >
        <Header icon="browser" content={this.props.name} />
        <Modal.Content>
          <ProductUpdate
            formData={updatedFormData}
            handleChange={this.handleChange}
          />
        </Modal.Content>
        <Modal.Actions>
          <Button color="green" onClick={this.handleChangeUpdate} inverted>
            <Icon name="checkmark" /> Update
          </Button>
          <Button color="red" onClick={this.handleDelete} inverted>
            <Icon name="checkmark" /> Delete
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default ProductModal;
