import React, { Component } from "react";
import { Form, Segment } from "semantic-ui-react";

import AuthContext from "../authContext";

class ProductUpdate extends Component {
  static contextType = AuthContext;

  render() {
    return (
      <Segment padded="very">
        <Form>
          <Form.Group widths="equal">
            <Form.Input
              fluid
              label="Product name"
              placeholder="Product name"
              onChange={this.props.handleChange("name")}
              required
            />
            <Form.Input
              fluid
              label="Version"
              placeholder="v12.3.2"
              onChange={this.props.handleChange("version")}
              required
            />
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Input
              fluid
              label="Build Number"
              placeholder="75637"
              onChange={this.props.handleChange("build")}
              required
            />
            <Form.Input
              fluid
              label="Build Type"
              placeholder="Beta"
              onChange={this.props.handleChange("build_type")}
              required
            />
          </Form.Group>
          <Form.TextArea
            label="Release Notes"
            placeholder="Tell us more about this release..."
            onChange={this.props.handleChange("release_notes")}
            required
          />
          <Form.Input
            fluid
            label="Download Link"
            placeholder="https://my.downloads.com"
            onChange={this.props.handleChange("download_link")}
            required
          />
        </Form>
      </Segment>
    );
  }
}

export default ProductUpdate;
