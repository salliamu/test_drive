import React, { Component } from "react";
import { Form, Segment, Button, Header } from "semantic-ui-react";

import AuthContext from "../authContext";

class AuthPage extends Component {
  state = {
    username: "",
    secret_code: "",
    isLogin: true,
  };

  static contextType = AuthContext;

  /**
   * Handles any change made in the input and updates the state accordingly.
   * Passes in prop name such that the names correspond to those in the state.
   */
  handleChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  /**
   * Switches between Login & Sign Up mode.
   */
  switchMode = () => {
    this.setState((prevState) => {
      return { isLogin: !prevState.isLogin };
    });
  };

  /**
   * Handles submitting of the information to the backend and
   * requests the creation of a new user.
   */
  handleSubmit = (e) => {
    e.preventDefault();
    const { username, secret_code } = this.state;

    let reqBody = {
      query: `
      query {
        login(username:"${username}", secret_code:"${secret_code}"){
          userId
          token
        }
      }
      `,
    };

    if (!this.state.isLogin) {
      reqBody = {
        query: `
        mutation {
          createUser(userInput: {username: "${username}" secret_code:"${secret_code}"}){
            _id
            username
            created_products{
              _id
            }
          }
        }
        `,
      };
    }

    fetch("http://localhost:8000/api", {
      method: "POST",
      body: JSON.stringify(reqBody),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then((resData) => {
        if (resData.data.login.token) {
          console.log(resData.data.login);
          this.context.login(
            resData.data.login.token,
            resData.data.login.userId
          );
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  render() {
    const { username, secret_code } = this.state;
    return (
      <div className="signup">
        <Segment inverted>
          <Form inverted onSubmit={this.handleSubmit}>
            <Header as="h2" inverted color="grey">
              {this.state.isLogin ? "Sign In" : "Sign Up"}
            </Header>
            <Form.Input
              label="username"
              placeholder="Username"
              name="username"
              value={username}
              onChange={this.handleChange}
              required
            />
            <Form.Input
              label="Secret Code"
              placeholder="secret code"
              name="secret_code"
              value={secret_code}
              onChange={this.handleChange}
              required
            />
            <Form.Button content={this.state.isLogin ? "Sign In" : "Sign Up"} />
            <Button onClick={this.switchMode}>
              {this.state.isLogin
                ? "Not Registered? Sign up"
                : "Registered? Sign In"}
            </Button>
          </Form>
        </Segment>
      </div>
    );
  }
}

export default AuthPage;
