import React, { Component } from "react";
import { Header, List } from "semantic-ui-react";

import ProductCard from "../components/productCard";

class HomePage extends Component {
  state = {
    products: [],
  };

  componentDidMount() {
    this.fetchProductList();
  }

  /**
   * Handles querying the api for the list of all products.
   * It then save the list to the state.
   */
  fetchProductList = () => {
    const reqBody = {
      query: `
      query {
        products{
          _id
          name
          version
          build
          build_type
          release_notes
          download_link
          owner{
            _id
          }
        }
      }
        `,
    };

    const token = this.context.token;
    fetch("http://localhost:8000/api", {
      method: "POST",
      body: JSON.stringify(reqBody),
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    })
      .then((res) => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then((resData) => {
        const products = resData.data.products;
        this.setState({ products: products });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  render() {
    /**
     * Extracts the fields from the products list and passes them down to the
     * ProductCard component which will display each product.
     */
    const productList = this.state.products.map((product) => {
      return (
        <ProductCard
          key={product._id}
          productId={product._id}
          name={product.name}
          version={product.version}
          build={product.build}
          build_type={product.build_type}
          release_notes={product.release_notes}
          download_link={product.download_link}
        />
      );
    });

    return (
      <div className="homepage">
        <div className="header">
          <Header as="h1" textAlign="left">
            The Software Downloads.
          </Header>
        </div>
        <div>
          <List floated="right">
            <List.Item as="h3">
              <a href="http://localhost:3000/auth">Manage Releases</a>
            </List.Item>
          </List>
        </div>
        <div className="productList">{productList}</div>
      </div>
    );
  }
}

export default HomePage;
