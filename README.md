# Test Drive

This project is a simple implementation of a web application for managing software downloads.  
It is based on NodeJS with a GraphQL api and a React frontend.

## Dependencies

Requires NodeJS as it uses npm as a package manager.  
To get started, clone the project from this repo.

Navigate to the server folder and install the dependencies and start the server.

```
cd server
npm install
npm start
```

In another terminal, navigate to the client folder and repeat as as bove to install the dependencies & start up the client.

```
cd client
npm install
npm start
```

The application should be running at `http://localhost:3000`

## Running the Program

All users are able to view all available software downloads in the home page.
In order to manage any downloads, one must log in. An authenticated user is then able to view all of their created downloads well as add new ones.
