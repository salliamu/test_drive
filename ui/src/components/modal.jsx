import React, { Component } from "react";
import { List, Label, Button, Icon, Header, Modal } from "semantic-ui-react";
import AuthContext from "../authContext";

class ProductModal extends Component {
  state = { modalOpen: false };

  static contextType = AuthContext;
  handleOpen = () => this.setState({ modalOpen: true });

  handleClose = () => this.setState({ modalOpen: false });

  handleDelete = () => {
    console.log(this.props.productId);
    const reqBody = {
      query: `
      mutation {
        deleteProduct(productId: "${this.props.productId}")
      }
          `,
    };

    fetch("http://localhost:8000/api", {
      method: "POST",
      body: JSON.stringify(reqBody),
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.context.token,
      },
    })
      .then((res) => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then((resData) => {
        const isDeleted = resData;
        this.setState({ modalOpen: !isDeleted });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  render() {
    return (
      <Modal
        trigger={
          <List.Item key={this.props.productId} onClick={this.handleOpen}>
            <List.Header>{this.props.name}</List.Header>
            <Label color="teal" horizontal>
              {this.props.version}
            </Label>
            {this.props.build_type}
          </List.Item>
        }
        open={this.state.modalOpen}
        onClose={this.handleClose}
        basic
        size="small"
      >
        <Header icon="browser" content="Release" />
        <Modal.Content></Modal.Content>
        <Modal.Actions>
          <Button color="green" onClick={this.handleClose} inverted>
            <Icon name="checkmark" /> Update
          </Button>
          <Button color="red" onClick={this.handleDelete} inverted>
            <Icon name="checkmark" /> Delete
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default ProductModal;
