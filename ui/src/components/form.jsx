import React, { Component } from "react";
import { Form, Segment } from "semantic-ui-react";
import AuthContext from "../authContext";
class ProductForm extends Component {
  static contextType = AuthContext;
  render() {
    /*  const options = [
      { key: "n", text: "Nightly", value: "nightly" },
      { key: "b", text: "Beta", value: "beta" },
      { key: "s", text: "Stable", value: "stable" },
    ]; */
    return (
      <Segment>
        <Form onSubmit={this.props.handleSubmit}>
          <Form.Group widths="equal">
            <Form.Input
              fluid
              label="Product name"
              placeholder="Product name"
              onChange={this.props.handleChange("name")}
              required
            />
            <Form.Input
              fluid
              label="Version"
              placeholder="v12.3.2"
              onChange={this.props.handleChange("version")}
            />
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Input
              fluid
              label="Build Number"
              placeholder="75637"
              onChange={this.props.handleChange("build")}
            />
            <Form.Input
              fluid
              label="Build Type"
              placeholder="Beta"
              onChange={this.props.handleChange("build_type")}
            />
            {/* <Form.Select
              fluid
              label="Build Type"
              placeholder="Beta"
              options={options}
            /> */}
          </Form.Group>
          <Form.TextArea
            label="Release Notes"
            placeholder="Tell us more about this release..."
            onChange={this.props.handleChange("release_notes")}
          />
          <Form.Input
            fluid
            label="Download Link"
            placeholder="https://my.downloads.com"
            onChange={this.props.handleChange("download_link")}
          />
          <Form.Button>Add New Release</Form.Button>
        </Form>
      </Segment>
    );
  }
}

export default ProductForm;
