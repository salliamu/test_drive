import React, { Component } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import AuthPage from "./pages/auth";
import HomePage from "./pages/home";
import AdminPage from "./pages/admin";
import AuthContext from "./authContext";
import "./App.css";

class App extends Component {
  state = {
    token: null,
    userId: null,
  };

  login = (token, userId) => {
    this.setState({ token: token, userId: userId });
  };

  logout = () => {
    this.setState({ token: null, userId: null });
  };

  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <AuthContext.Provider
            value={{
              token: this.state.token,
              userId: this.state.userId,
              login: this.login,
              logout: this.logout,
            }}
          >
            <Switch>
              {!this.state.token && <Redirect from="/admin" to="/auth" exact />}
              {this.state.token && (
                <Route path="/admin" component={AdminPage} />
              )}
              {this.state.token && <Redirect from="/auth" to="/admin" exact />}
              {!this.state.token && <Route path="/auth" component={AuthPage} />}
              <Route path="/" component={HomePage} />
            </Switch>
          </AuthContext.Provider>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
