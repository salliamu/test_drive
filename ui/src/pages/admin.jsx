import React, { Component } from "react";
import { Grid, List, Segment, Header } from "semantic-ui-react";

import AuthContext from "../authContext";
import ProductModal from "../components/modal";
import ProductForm from "../components/form";

class AdminPage extends Component {
  state = {
    formData: {
      name: "",
      version: "",
      build: "",
      build_type: "",
      release_notes: "",
      download_link: "",
    },
    createdProducts: [],
  };

  static contextType = AuthContext;

  componentDidMount() {
    this.fetchCreatedProducts();
  }

  fetchCreatedProducts = () => {
    const reqBody = {
      query: `
      query {
        user{
          username
          created_products{
            _id
            name
            version
            build_type
          }
        }
      }
        `,
    };

    fetch("http://localhost:8000/api", {
      method: "POST",
      body: JSON.stringify(reqBody),
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.context.token,
      },
    })
      .then((res) => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then((resData) => {
        const createdProducts = resData.data.user.created_products;
        const user = resData.data.user.username;
        this.setState({ createdProducts: createdProducts, username: user });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleChange = (input) => (event) => {
    var formData = this.state.formData;
    formData[input] = event.target.value;
    this.setState(formData);
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const formData = this.state.formData;

    const reqBody = {
      query: `
      mutation {
        createProduct(productInput: {name:"${formData.name}", version:"${formData.version}", build:"${formData.build}", build_type:"${formData.build_type}", release_notes:"${formData.release_notes}", download_link:"${formData.download_link}"}){
          name
          version
          build
          build_type
          release_notes
          download_link
        }
      }
        `,
    };
    fetch("http://localhost:8000/api", {
      method: "POST",
      body: JSON.stringify(reqBody),
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.context.token,
      },
    })
      .then((res) => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then((resData) => {
        console.log(resData);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  render() {
    const createdProducts = this.state.createdProducts.map((product) => {
      return (
        <ProductModal
          key={product._id}
          productId={product._id}
          name={product.name}
          version={product.version}
          build={product.build}
          build_type={product.build_type}
          release_notes={product.release_notes}
          download_link={product.download_link}
        />
      );
    });
    const formData = this.state.formData;

    return (
      <div className="adminPage">
        <Header as="h1" textAlign="right">
          Hi, {this.state.username}!
        </Header>
        <Grid columns={2}>
          <Grid.Row>
            <Grid.Column width={4}>
              <List animated verticalAlign="middle">
                {createdProducts}
              </List>
            </Grid.Column>
            <Grid.Column width={12}>
              <Segment>
                <ProductForm
                  handleChange={this.handleChange}
                  formData={formData}
                  handleSubmit={this.handleSubmit}
                />
              </Segment>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

export default AdminPage;
